import java.util.Scanner;

public class CountString {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Introduceti un text");
        String m = s.nextLine();
        sumCharacter(m);
    }

    private static void sumCharacter(String m) {
        String lower = m.toLowerCase();
        int sumvocale = 0;
        int sumconsoane = 0;
        int sumspatiu = 0;
        int sumcifre = 0;
        int punctmarks = 0;
        for (int i = 0; i < lower.length(); i++) {
            char caracter = lower.charAt(i);
            System.out.println(caracter);
            if (caracter == 'a' || caracter == 'e' || caracter == 'i' || caracter == 'o' || caracter == 'u') {
                System.out.println(" este vocala");
                sumvocale = sumvocale + 1;
            } else if (caracter == ' ') {
                System.out.println(" este spatiu");
                sumspatiu = sumspatiu + 1;
            } else if (caracter == ',' || caracter == '!' || caracter == '?' || caracter == '.') {
                System.out.println(" este semn de punctuatie");
                punctmarks = punctmarks + 1;
            } else if (caracter == '0' || caracter == '1' || caracter == '2' || caracter == '3' || caracter == '4' || caracter == '5' || caracter == '6' || caracter == '7' || caracter == '8' || caracter == '9') {
                System.out.println(" este numar");
                sumconsoane = sumconsoane + 1;
            } else {
                System.out.println(" este consoana");
                sumconsoane = sumconsoane + 1;
            }

        }
        System.out.println("numarul de vocale este " + sumvocale);
        System.out.println("numarul de consoane este " + sumconsoane);
        System.out.println("numarul de cifre este " + sumcifre);
        System.out.println("numarul de spatii este " + sumspatiu);
        System.out.println("numarul de semne de punctuatie este " + punctmarks);
    }
}
